XeTex-docker
============

This project stores Docker images used to build PDFs. Each variation is put
under its own directory.

If you only want to use the image, check out [container registery page](https://gitlab.com/uiseg/xetex-docker/container_registry).
